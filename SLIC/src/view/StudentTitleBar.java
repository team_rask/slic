/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import methords.LoadWindow;

/**
 *
 * @author ASUS
 */
public class StudentTitleBar extends javax.swing.JPanel {

    /**
     * Creates new form ButtonBar
     */
    public StudentTitleBar() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelStuTitleBar = new javax.swing.JPanel();
        jPanelRegister = new javax.swing.JPanel();
        lblRegister = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanelRegister1 = new javax.swing.JPanel();
        lblAssignSub = new javax.swing.JLabel();
        jPanelRegister4 = new javax.swing.JPanel();
        lblView = new javax.swing.JLabel();
        jPanelRegister2 = new javax.swing.JPanel();
        lblEdit = new javax.swing.JLabel();
        jPanelRegister3 = new javax.swing.JPanel();
        lblEdit1 = new javax.swing.JLabel();

        jPanelStuTitleBar.setBackground(new java.awt.Color(0, 0, 51));

        jPanelRegister.setBackground(new java.awt.Color(0, 51, 204));
        jPanelRegister.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanelRegister.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanelRegisterMouseClicked(evt);
            }
        });

        lblRegister.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblRegister.setForeground(new java.awt.Color(255, 255, 255));
        lblRegister.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblRegister.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/buttonbar/icons8_Add_User_Male_32px.png"))); // NOI18N
        lblRegister.setText("REGISTER");
        lblRegister.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblRegisterMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanelRegisterLayout = new javax.swing.GroupLayout(jPanelRegister);
        jPanelRegister.setLayout(jPanelRegisterLayout);
        jPanelRegisterLayout.setHorizontalGroup(
            jPanelRegisterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRegisterLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblRegister, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanelRegisterLayout.setVerticalGroup(
            jPanelRegisterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblRegister, javax.swing.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
        );

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel2.setText("STUDENT");

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel3.setText("Management");

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/buttonbar/icons8_Back_To_28px.png"))); // NOI18N

        jPanelRegister1.setBackground(new java.awt.Color(0, 51, 204));
        jPanelRegister1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanelRegister1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanelRegister1MouseClicked(evt);
            }
        });

        lblAssignSub.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblAssignSub.setForeground(new java.awt.Color(255, 255, 255));
        lblAssignSub.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblAssignSub.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/buttonbar/icons8_Find_User_Male_32px_1.png"))); // NOI18N
        lblAssignSub.setText("ASSIGN SUBJECT");
        lblAssignSub.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblAssignSubMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanelRegister1Layout = new javax.swing.GroupLayout(jPanelRegister1);
        jPanelRegister1.setLayout(jPanelRegister1Layout);
        jPanelRegister1Layout.setHorizontalGroup(
            jPanelRegister1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRegister1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAssignSub, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelRegister1Layout.setVerticalGroup(
            jPanelRegister1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblAssignSub, javax.swing.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
        );

        jPanelRegister4.setBackground(new java.awt.Color(0, 51, 204));
        jPanelRegister4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanelRegister4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanelRegister4MouseClicked(evt);
            }
        });

        lblView.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblView.setForeground(new java.awt.Color(255, 255, 255));
        lblView.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblView.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/buttonbar/icons8_User_Groups_32px.png"))); // NOI18N
        lblView.setText("VIEW ALL");
        lblView.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblViewMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanelRegister4Layout = new javax.swing.GroupLayout(jPanelRegister4);
        jPanelRegister4.setLayout(jPanelRegister4Layout);
        jPanelRegister4Layout.setHorizontalGroup(
            jPanelRegister4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRegister4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblView, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanelRegister4Layout.setVerticalGroup(
            jPanelRegister4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblView, javax.swing.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
        );

        jPanelRegister2.setBackground(new java.awt.Color(0, 51, 204));
        jPanelRegister2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanelRegister2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanelRegister2MouseClicked(evt);
            }
        });

        lblEdit.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblEdit.setForeground(new java.awt.Color(255, 255, 255));
        lblEdit.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/buttonbar/icons8_Registration_32px_1.png"))); // NOI18N
        lblEdit.setText("EDIT");
        lblEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblEditMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanelRegister2Layout = new javax.swing.GroupLayout(jPanelRegister2);
        jPanelRegister2.setLayout(jPanelRegister2Layout);
        jPanelRegister2Layout.setHorizontalGroup(
            jPanelRegister2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRegister2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelRegister2Layout.setVerticalGroup(
            jPanelRegister2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblEdit, javax.swing.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
        );

        jPanelRegister3.setBackground(new java.awt.Color(0, 51, 204));
        jPanelRegister3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanelRegister3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanelRegister3MouseClicked(evt);
            }
        });

        lblEdit1.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblEdit1.setForeground(new java.awt.Color(255, 255, 255));
        lblEdit1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblEdit1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/buttonbar/icons8_Denied_32px.png"))); // NOI18N
        lblEdit1.setText("REMOVE");
        lblEdit1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblEdit1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanelRegister3Layout = new javax.swing.GroupLayout(jPanelRegister3);
        jPanelRegister3.setLayout(jPanelRegister3Layout);
        jPanelRegister3Layout.setHorizontalGroup(
            jPanelRegister3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRegister3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblEdit1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelRegister3Layout.setVerticalGroup(
            jPanelRegister3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblEdit1, javax.swing.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanelStuTitleBarLayout = new javax.swing.GroupLayout(jPanelStuTitleBar);
        jPanelStuTitleBar.setLayout(jPanelStuTitleBarLayout);
        jPanelStuTitleBarLayout.setHorizontalGroup(
            jPanelStuTitleBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelStuTitleBarLayout.createSequentialGroup()
                .addComponent(jPanelRegister, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelRegister1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelRegister4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(jPanelRegister2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelRegister3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelStuTitleBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanelStuTitleBarLayout.setVerticalGroup(
            jPanelStuTitleBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelRegister, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelStuTitleBarLayout.createSequentialGroup()
                .addGroup(jPanelStuTitleBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanelStuTitleBarLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(2, 2, 2))
            .addComponent(jPanelRegister1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanelRegister4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanelRegister2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanelRegister3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelStuTitleBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanelStuTitleBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jPanelRegisterMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelRegisterMouseClicked
        LoadWindow.setStudentTitle();
    }//GEN-LAST:event_jPanelRegisterMouseClicked

    private void lblRegisterMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblRegisterMouseClicked
        LoadWindow.setAddStudent();
    }//GEN-LAST:event_lblRegisterMouseClicked

    private void lblAssignSubMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAssignSubMouseClicked
        LoadWindow.setStudentAssignSubject();
    }//GEN-LAST:event_lblAssignSubMouseClicked

    private void jPanelRegister1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelRegister1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanelRegister1MouseClicked

    private void lblEditMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblEditMouseClicked
        LoadWindow.setSearchStudent();
    }//GEN-LAST:event_lblEditMouseClicked

    private void jPanelRegister2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelRegister2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanelRegister2MouseClicked

    private void lblViewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblViewMouseClicked
        LoadWindow.setViewAllStudent();
    }//GEN-LAST:event_lblViewMouseClicked

    private void jPanelRegister4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelRegister4MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanelRegister4MouseClicked

    private void lblEdit1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblEdit1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_lblEdit1MouseClicked

    private void jPanelRegister3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanelRegister3MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanelRegister3MouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanelRegister;
    private javax.swing.JPanel jPanelRegister1;
    private javax.swing.JPanel jPanelRegister2;
    private javax.swing.JPanel jPanelRegister3;
    private javax.swing.JPanel jPanelRegister4;
    private javax.swing.JPanel jPanelStuTitleBar;
    private javax.swing.JLabel lblAssignSub;
    private javax.swing.JLabel lblEdit;
    private javax.swing.JLabel lblEdit1;
    private javax.swing.JLabel lblRegister;
    private javax.swing.JLabel lblView;
    // End of variables declaration//GEN-END:variables
}

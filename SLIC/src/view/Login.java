package view;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import methords.StartMethords;
import recapture.Recapture;

/**
 *
 * @author Dasun
 */
public class Login extends javax.swing.JFrame {

    int code1, code2, code3, code4, code5, code6;
    int buttonClick;
    int count;
    int wrongPassCount;

    ImageIcon filled = new ImageIcon(getClass().getResource("/resources/images/icons/login/circle_filled.png"));
    ImageIcon unfilled = new ImageIcon(getClass().getResource("/resources/images/icons/login/circle_unfilled.png"));
    ImageIcon logo = new ImageIcon(getClass().getResource("/resources/images/logo/logo.png"));
    private int xMouse;
    private int yMouse;

    public Login() {
        initComponents();
        runTimeAutoSet();
        setLocationRelativeTo(null);
        this.setIconImage(logo.getImage());
        loader();
    }

    private void loader() {
        try {
            InetAddress iAddress = InetAddress.getLocalHost();
            String hostName = iAddress.getHostName();
            hostNameText.setText(hostName.toUpperCase());
        } catch (UnknownHostException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static String convertTo24Hour(String dateAndTime) {
        DateFormat f1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //11:00 pm
        Date d = null;
        try {
            d = f1.parse(dateAndTime);
        } catch (ParseException e) {
        }
        DateFormat f2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        String x = f2.format(d); // "23:00"

        return x;
    }

    private class MyKeyListener extends KeyAdapter {

        @Override
        @SuppressWarnings("deprecation")
        public void keyPressed(KeyEvent e) {
//Alt+F4 Default set
            if (e.getKeyCode() == KeyEvent.VK_NUMPAD1) {
                buttonClick = 1;
                circlecontrol();
            }
            if (e.getKeyCode() == KeyEvent.VK_NUMPAD2) {
                buttonClick = 2;
                circlecontrol();
            }
            if (e.getKeyCode() == KeyEvent.VK_NUMPAD3) {
                buttonClick = 3;
                circlecontrol();
            }
            if (e.getKeyCode() == KeyEvent.VK_NUMPAD4) {
                buttonClick = 4;
                circlecontrol();
            }
            if (e.getKeyCode() == KeyEvent.VK_NUMPAD5) {
                buttonClick = 5;
                circlecontrol();
            }
            if (e.getKeyCode() == KeyEvent.VK_NUMPAD6) {
                buttonClick = 6;
                circlecontrol();
            }
            if (e.getKeyCode() == KeyEvent.VK_NUMPAD7) {
                buttonClick = 7;
                circlecontrol();
            }
            if (e.getKeyCode() == KeyEvent.VK_NUMPAD8) {
                buttonClick = 8;
                circlecontrol();
            }
            if (e.getKeyCode() == KeyEvent.VK_NUMPAD9) {
                buttonClick = 9;
                circlecontrol();
            }
            if (e.getKeyCode() == KeyEvent.VK_NUMPAD0) {
                buttonClick = 0;
                circlecontrol();
            }
        }
    }

    public void circlecontrol() {
        count++;
        switch (count) {
            case 1:
                Code1.setIcon(filled);
                code1 = buttonClick;

                break;
            case 2:
                Code2.setIcon(filled);
                code2 = buttonClick;
                break;
            case 3:
                Code3.setIcon(filled);
                code3 = buttonClick;
                break;
            case 4:
                Code4.setIcon(filled);
                code4 = buttonClick;
                break;
            case 5:
                Code5.setIcon(filled);
                code5 = buttonClick;
                break;
            case 6:
                Code6.setIcon(filled);
                code6 = buttonClick;

                String masterTest = code1 + "" + code2 + "" + code3 + "" + code4 + "" + code5 + "" + code6;
//Passcode Checker and Active User
                boolean test = StartMethords.selectedUserPassCheck(txtUserName, masterTest);
                if (test) {
                    this.dispose();
                    System.gc();
                    new Hi().setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(this, "Wrong Username or Passcode", "ERROR", JOptionPane.WARNING_MESSAGE);
                    wrongPassCount++;
                    captch();
                    // timerC();
                }
                Code1.setIcon(unfilled);
                Code2.setIcon(unfilled);
                Code3.setIcon(unfilled);
                Code4.setIcon(unfilled);
                Code5.setIcon(unfilled);
                Code6.setIcon(unfilled);
                count = 0;
                break;
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        User_Icon = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        copyrights = new javax.swing.JLabel();
        countDown = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        hostNameText = new javax.swing.JLabel();
        txtUserName = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        no1 = new javax.swing.JLabel();
        no2 = new javax.swing.JLabel();
        no3 = new javax.swing.JLabel();
        no4 = new javax.swing.JLabel();
        no5 = new javax.swing.JLabel();
        no6 = new javax.swing.JLabel();
        no7 = new javax.swing.JLabel();
        no8 = new javax.swing.JLabel();
        no9 = new javax.swing.JLabel();
        no10 = new javax.swing.JLabel();
        Code6 = new javax.swing.JLabel();
        Code5 = new javax.swing.JLabel();
        Code4 = new javax.swing.JLabel();
        Code3 = new javax.swing.JLabel();
        Code1 = new javax.swing.JLabel();
        Code2 = new javax.swing.JLabel();
        Heading = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        FrameDrag = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("LOGIN");
        setAlwaysOnTop(true);
        setUndecorated(true);

        jPanel1.setLayout(null);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);

        jLabel1.setBackground(new java.awt.Color(51, 51, 51));
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("jLabel1");
        jLabel1.setOpaque(true);
        jPanel2.add(jLabel1);
        jLabel1.setBounds(-120, -4, 1110, 20);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(0, 0, 620, 10);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(null);

        jLabel4.setBackground(new java.awt.Color(51, 51, 51));
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("jLabel1");
        jLabel4.setOpaque(true);
        jPanel4.add(jLabel4);
        jLabel4.setBounds(-40, -10, 1110, 30);

        jPanel1.add(jPanel4);
        jPanel4.setBounds(0, 440, 848, 10);

        jPanel3.setBackground(new java.awt.Color(51, 51, 51));
        jPanel3.setLayout(null);

        User_Icon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/login/Super User Big.png"))); // NOI18N
        User_Icon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                User_IconMouseClicked(evt);
            }
        });
        jPanel3.add(User_Icon);
        User_Icon.setBounds(110, 80, 100, 130);

        jLabel35.setDisplayedMnemonic('U');
        jLabel35.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(204, 204, 204));
        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel35.setText("LOGIN");
        jPanel3.add(jLabel35);
        jLabel35.setBounds(30, 20, 150, 40);

        copyrights.setBackground(new java.awt.Color(255, 255, 255));
        copyrights.setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        copyrights.setForeground(new java.awt.Color(102, 102, 102));
        copyrights.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        copyrights.setText("© YEAR iTeach™ Products. All right reserved ");
        jPanel3.add(copyrights);
        copyrights.setBounds(0, 400, 330, 14);

        countDown.setFont(new java.awt.Font("Arial", 0, 21)); // NOI18N
        countDown.setForeground(new java.awt.Color(255, 255, 255));
        countDown.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel3.add(countDown);
        countDown.setBounds(0, 350, 330, 40);

        jLabel7.setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(177, 177, 177));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("* Enter Username Correctly");
        jPanel3.add(jLabel7);
        jLabel7.setBounds(50, 260, 220, 30);

        hostNameText.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        hostNameText.setForeground(new java.awt.Color(153, 153, 153));
        hostNameText.setText("iTeach PC");
        jPanel3.add(hostNameText);
        hostNameText.setBounds(140, 330, 180, 20);

        txtUserName.setBackground(new java.awt.Color(204, 204, 204));
        txtUserName.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        txtUserName.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtUserName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUserNameActionPerformed(evt);
            }
        });
        jPanel3.add(txtUserName);
        txtUserName.setBounds(50, 220, 220, 40);

        jLabel8.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(177, 177, 177));
        jLabel8.setText("Local Server   :");
        jPanel3.add(jLabel8);
        jLabel8.setBounds(50, 330, 90, 20);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(0, 10, 330, 430);

        no1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        no1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                no1MouseReleased(evt);
            }
        });
        jPanel1.add(no1);
        no1.setBounds(376, 147, 49, 49);

        no2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        no2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                no2MouseReleased(evt);
            }
        });
        jPanel1.add(no2);
        no2.setBounds(447, 147, 49, 49);

        no3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        no3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                no3MouseReleased(evt);
            }
        });
        jPanel1.add(no3);
        no3.setBounds(514, 147, 49, 49);

        no4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        no4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                no4MouseReleased(evt);
            }
        });
        jPanel1.add(no4);
        no4.setBounds(376, 205, 49, 49);

        no5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        no5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                no5MouseReleased(evt);
            }
        });
        jPanel1.add(no5);
        no5.setBounds(446, 205, 49, 49);

        no6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        no6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                no6MouseReleased(evt);
            }
        });
        jPanel1.add(no6);
        no6.setBounds(515, 205, 49, 49);

        no7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        no7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                no7MouseReleased(evt);
            }
        });
        jPanel1.add(no7);
        no7.setBounds(376, 263, 49, 49);

        no8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        no8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                no8MouseReleased(evt);
            }
        });
        jPanel1.add(no8);
        no8.setBounds(446, 263, 49, 49);

        no9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        no9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                no9MouseReleased(evt);
            }
        });
        jPanel1.add(no9);
        no9.setBounds(515, 263, 49, 49);

        no10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        no10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                no10MouseReleased(evt);
            }
        });
        jPanel1.add(no10);
        no10.setBounds(446, 322, 49, 49);

        Code6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/login/circle_unfilled.png"))); // NOI18N
        jPanel1.add(Code6);
        Code6.setBounds(540, 70, 20, 20);

        Code5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/login/circle_unfilled.png"))); // NOI18N
        jPanel1.add(Code5);
        Code5.setBounds(510, 70, 20, 20);

        Code4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/login/circle_unfilled.png"))); // NOI18N
        jPanel1.add(Code4);
        Code4.setBounds(480, 70, 20, 20);

        Code3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/login/circle_unfilled.png"))); // NOI18N
        jPanel1.add(Code3);
        Code3.setBounds(450, 70, 20, 20);

        Code1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/login/circle_unfilled.png"))); // NOI18N
        jPanel1.add(Code1);
        Code1.setBounds(390, 70, 20, 20);

        Code2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/login/circle_unfilled.png"))); // NOI18N
        jPanel1.add(Code2);
        Code2.setBounds(420, 70, 20, 20);

        Heading.setFont(new java.awt.Font("Segoe UI Light", 0, 19)); // NOI18N
        Heading.setForeground(new java.awt.Color(255, 255, 255));
        Heading.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Heading.setText("Enter Passcode");
        jPanel1.add(Heading);
        Heading.setBounds(410, 95, 130, 30);

        jLabel3.setFont(new java.awt.Font("Web Symbols", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(204, 204, 204));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/login/icons8_Shutdown_28px_1.png"))); // NOI18N
        jLabel3.setToolTipText("Close");
        jLabel3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel3);
        jLabel3.setBounds(570, 0, 40, 50);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/login/PassCode2.png"))); // NOI18N
        jPanel1.add(jLabel2);
        jLabel2.setBounds(340, 120, 270, 290);

        FrameDrag.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                FrameDragMouseDragged(evt);
            }
        });
        FrameDrag.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                FrameDragMousePressed(evt);
            }
        });
        jPanel1.add(FrameDrag);
        FrameDrag.setBounds(0, -6, 610, 80);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(102, 102, 102));
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/login/Login_Background.png"))); // NOI18N
        jPanel1.add(jLabel5);
        jLabel5.setBounds(330, 0, 290, 440);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 610, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 449, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void FrameDragMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_FrameDragMouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();

        this.setLocation(x - xMouse, y - yMouse);
    }//GEN-LAST:event_FrameDragMouseDragged

    private void FrameDragMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_FrameDragMousePressed
        xMouse = evt.getX();
        yMouse = evt.getY();
    }//GEN-LAST:event_FrameDragMousePressed

    private void User_IconMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_User_IconMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_User_IconMouseClicked

    private void no1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_no1MouseReleased
        buttonClick = 1;
        circlecontrol();
    }//GEN-LAST:event_no1MouseReleased

    private void no2MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_no2MouseReleased
        buttonClick = 2;
        circlecontrol();
    }//GEN-LAST:event_no2MouseReleased

    private void no3MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_no3MouseReleased
        buttonClick = 3;
        circlecontrol();
    }//GEN-LAST:event_no3MouseReleased

    private void no4MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_no4MouseReleased
        buttonClick = 4;
        circlecontrol();
    }//GEN-LAST:event_no4MouseReleased

    private void no5MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_no5MouseReleased
        buttonClick = 5;
        circlecontrol();
    }//GEN-LAST:event_no5MouseReleased

    private void no6MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_no6MouseReleased
        buttonClick = 6;
        circlecontrol();
    }//GEN-LAST:event_no6MouseReleased

    private void no7MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_no7MouseReleased
        buttonClick = 7;
        circlecontrol();
    }//GEN-LAST:event_no7MouseReleased

    private void no8MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_no8MouseReleased
        buttonClick = 8;
        circlecontrol();
    }//GEN-LAST:event_no8MouseReleased

    private void no9MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_no9MouseReleased
        buttonClick = 9;
        circlecontrol();
    }//GEN-LAST:event_no9MouseReleased

    private void no10MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_no10MouseReleased
        buttonClick = 0;
        circlecontrol();
    }//GEN-LAST:event_no10MouseReleased

    private void txtUserNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUserNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtUserNameActionPerformed

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked
        System.exit(0);
        System.gc();
    }//GEN-LAST:event_jLabel3MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new Login().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Code1;
    private javax.swing.JLabel Code2;
    private javax.swing.JLabel Code3;
    private javax.swing.JLabel Code4;
    private javax.swing.JLabel Code5;
    private javax.swing.JLabel Code6;
    private javax.swing.JLabel FrameDrag;
    private javax.swing.JLabel Heading;
    private javax.swing.JLabel User_Icon;
    private javax.swing.JLabel copyrights;
    private javax.swing.JLabel countDown;
    private javax.swing.JLabel hostNameText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel no1;
    private javax.swing.JLabel no10;
    private javax.swing.JLabel no2;
    private javax.swing.JLabel no3;
    private javax.swing.JLabel no4;
    private javax.swing.JLabel no5;
    private javax.swing.JLabel no6;
    private javax.swing.JLabel no7;
    private javax.swing.JLabel no8;
    private javax.swing.JLabel no9;
    private javax.swing.JTextField txtUserName;
    // End of variables declaration//GEN-END:variables

    private void allDiasable() {
        txtUserName.setEnabled(false);
        no1.setEnabled(false);
        no2.setEnabled(false);
        no3.setEnabled(false);
        no4.setEnabled(false);
        no5.setEnabled(false);
        no6.setEnabled(false);
        no7.setEnabled(false);
        no8.setEnabled(false);
        no9.setEnabled(false);
        no10.setEnabled(false);
    }

    private void allEnable() {
        txtUserName.setEnabled(true);
        no1.setEnabled(true);
        no2.setEnabled(true);
        no3.setEnabled(true);
        no4.setEnabled(true);
        no5.setEnabled(true);
        no6.setEnabled(true);
        no7.setEnabled(true);
        no8.setEnabled(true);
        no9.setEnabled(true);
        no10.setEnabled(true);
    }

    private void timerC() {
        if (wrongPassCount >= 3) {
            allDiasable();
            int i = 5;
            while (i > 0) {
                try {
                    countDown.setText("Try again in " + i + " seconds.");
                    System.out.println("Try again in " + i + " seconds.");
                    i--;
                    Thread.sleep(1000L);
                } catch (InterruptedException ex) {
                    System.out.println(ex.getMessage());
                }
            }
            wrongPassCount = 0;
            allEnable();
            countDown.setText("");
        }
    }

    private void captch() {
        if (wrongPassCount >= 3) {
            new Recapture().setVisible(true);
            this.dispose();
            System.gc();
        }
    }

    private void runTimeAutoSet() {
        String thisYear = getYear();
        copyrights.setText("© " + thisYear + " iTeach™ Products. All right reserved ");
    }

    //Get This Year
    public static String getYear() {
        SimpleDateFormat year = new SimpleDateFormat("YYYY");
        String thisYear = (year.format(new Date()));
        return thisYear;
    }

}

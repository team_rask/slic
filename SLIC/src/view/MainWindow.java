/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.ImageIcon;
import methords.LoadWindow;
import methords.StartMethords;

/**
 *
 * @author ASUS
 */
public class MainWindow extends javax.swing.JFrame {

    /**
     * Creates new form Menu
     */
    public MainWindow() {
        initComponents();
        setLocationRelativeTo(null);
        ImageIcon logo = new ImageIcon(getClass().getResource("/resources/images/logo/logo.png"));
        this.setIconImage(logo.getImage());
        runTimeAutoSet();
        setTime();
        loader();

    }

    public final void setTime() {
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1000);
                        SimpleDateFormat time = new SimpleDateFormat("hh:mm:ss");
                        SimpleDateFormat timeV = new SimpleDateFormat("a");
                        SimpleDateFormat date = new SimpleDateFormat("EEE / d");
                        String t = date.format(new Date()).toUpperCase();
                        SimpleDateFormat month = new SimpleDateFormat("MMMMM");
                        String m = month.format(new Date()).toUpperCase();
                        Clock.setText(time.format(new Date()));
                        AM_PM.setText(timeV.format(new Date()));
                        Date.setText(t);
                        Month.setText(m);
                    } catch (InterruptedException ex) {
                        System.out.println(ex.getMessage());
                        //  Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }.start();
    }

    private void runTimeAutoSet() {
        String thisYear = getYear();
        copyrights.setText("© " + thisYear + " iTeach™ Products. All right reserved ");
    }

    //Get This Year
    public static String getYear() {
        SimpleDateFormat year = new SimpleDateFormat("YYYY");
        String thisYear = (year.format(new Date()));
        return thisYear;
    }

    private void loader() {
        try {
            InetAddress iAddress = InetAddress.getLocalHost();
            String hostName = iAddress.getHostName();
            hostNameText.setText(hostName.toUpperCase());
            lblUser.setText("Hi!, " + StartMethords.loginUserName());
        } catch (UnknownHostException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        lblPaymentIcon = new javax.swing.JLabel();
        lblPayment = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        lblUser1Icon = new javax.swing.JLabel();
        lblProfile = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        lblFacebook = new javax.swing.JLabel();
        lblWeb = new javax.swing.JLabel();
        lblGoogle = new javax.swing.JLabel();
        lblYouTube = new javax.swing.JLabel();
        lblTwitter = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        lblStudentIcon = new javax.swing.JLabel();
        lblStudent = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        lblTitileBar = new javax.swing.JLabel();
        lblClose = new javax.swing.JLabel();
        lblMinimize = new javax.swing.JLabel();
        lblUser = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        copyrights = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        hostNameText = new javax.swing.JLabel();
        AM_PM = new javax.swing.JLabel();
        Clock = new javax.swing.JLabel();
        Date = new javax.swing.JLabel();
        Month = new javax.swing.JLabel();
        jPanelMain = new javax.swing.JPanel();
        jPanelBar = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/logo/logo.png"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(39, 350, 110, 110));

        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("www.iteach.lk");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 510, 194, -1));

        jPanel5.setBackground(new java.awt.Color(51, 51, 51));
        jPanel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel5MouseClicked(evt);
            }
        });

        lblPaymentIcon.setBackground(new java.awt.Color(0, 0, 0));
        lblPaymentIcon.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblPaymentIcon.setForeground(new java.awt.Color(102, 102, 102));
        lblPaymentIcon.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPaymentIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Card_Payment_28px.png"))); // NOI18N
        lblPaymentIcon.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblPaymentIcon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPaymentIconMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblPaymentIconMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblPaymentIconMouseExited(evt);
            }
        });

        lblPayment.setBackground(new java.awt.Color(0, 0, 0));
        lblPayment.setFont(new java.awt.Font("Segoe UI", 1, 22)); // NOI18N
        lblPayment.setForeground(java.awt.Color.gray);
        lblPayment.setText("PAYMENTS");
        lblPayment.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblPayment.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPaymentMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblPaymentMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblPaymentMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblPaymentIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(lblPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblPayment, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
            .addComponent(lblPaymentIcon, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 260, -1, -1));

        jPanel11.setBackground(new java.awt.Color(51, 51, 51));
        jPanel11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel11MouseClicked(evt);
            }
        });

        lblUser1Icon.setBackground(new java.awt.Color(0, 0, 0));
        lblUser1Icon.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblUser1Icon.setForeground(new java.awt.Color(102, 102, 102));
        lblUser1Icon.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblUser1Icon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Change_User_28px.png"))); // NOI18N
        lblUser1Icon.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblUser1Icon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblUser1IconMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblUser1IconMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblUser1IconMouseExited(evt);
            }
        });

        lblProfile.setBackground(new java.awt.Color(0, 0, 0));
        lblProfile.setFont(new java.awt.Font("Segoe UI", 1, 22)); // NOI18N
        lblProfile.setForeground(java.awt.Color.gray);
        lblProfile.setText("TEACHER");
        lblProfile.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblProfile.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblProfileMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblProfileMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblProfileMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblUser1Icon, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(lblProfile, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblUser1Icon, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
            .addComponent(lblProfile, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 190, -1, -1));

        jPanel12.setBackground(new java.awt.Color(0, 0, 0));

        lblFacebook.setBackground(new java.awt.Color(0, 0, 0));
        lblFacebook.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblFacebook.setForeground(new java.awt.Color(102, 102, 102));
        lblFacebook.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblFacebook.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Facebook_28px.png"))); // NOI18N

        lblWeb.setBackground(new java.awt.Color(0, 0, 0));
        lblWeb.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblWeb.setForeground(new java.awt.Color(102, 102, 102));
        lblWeb.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblWeb.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Website_28px.png"))); // NOI18N

        lblGoogle.setBackground(new java.awt.Color(0, 0, 0));
        lblGoogle.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblGoogle.setForeground(new java.awt.Color(102, 102, 102));
        lblGoogle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblGoogle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Google_Plus_28px.png"))); // NOI18N

        lblYouTube.setBackground(new java.awt.Color(0, 0, 0));
        lblYouTube.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblYouTube.setForeground(new java.awt.Color(102, 102, 102));
        lblYouTube.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblYouTube.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Play_Button_28px.png"))); // NOI18N

        lblTwitter.setBackground(new java.awt.Color(0, 0, 0));
        lblTwitter.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblTwitter.setForeground(new java.awt.Color(102, 102, 102));
        lblTwitter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTwitter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Twitter_Squared_28px.png"))); // NOI18N

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblFacebook)
                .addGap(5, 5, 5)
                .addComponent(lblGoogle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblWeb, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblYouTube, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTwitter, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblGoogle, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
            .addComponent(lblWeb, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(lblYouTube, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(lblTwitter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(lblFacebook, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 551, -1, -1));

        jPanel13.setBackground(new java.awt.Color(51, 51, 51));
        jPanel13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel13MouseClicked(evt);
            }
        });

        lblStudentIcon.setBackground(new java.awt.Color(0, 0, 0));
        lblStudentIcon.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblStudentIcon.setForeground(new java.awt.Color(102, 102, 102));
        lblStudentIcon.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblStudentIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Students_28px.png"))); // NOI18N
        lblStudentIcon.setToolTipText("STUDENTS");
        lblStudentIcon.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblStudentIcon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblStudentIconMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblStudentIconMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblStudentIconMouseExited(evt);
            }
        });

        lblStudent.setBackground(new java.awt.Color(0, 0, 0));
        lblStudent.setFont(new java.awt.Font("Segoe UI", 1, 22)); // NOI18N
        lblStudent.setForeground(java.awt.Color.gray);
        lblStudent.setText("STUDENTS");
        lblStudent.setToolTipText("STUDENTS");
        lblStudent.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblStudent.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblStudentMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblStudentMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblStudentMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addComponent(lblStudentIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblStudent, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblStudent, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
            .addComponent(lblStudentIcon, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 120, -1, -1));

        jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("www.iteach.lk");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(63, 40, 120, 30));

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(153, 153, 153));
        jLabel2.setText("iTeach 4U");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 3, 120, 60));

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/logo/icons8_Graduation_Cap_48px.png"))); // NOI18N
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 5, 69, 80));

        jPanel7.setBackground(new java.awt.Color(51, 51, 51));

        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Website_28px.png"))); // NOI18N
        jLabel4.setText("www.iteach.lk");

        lblTitileBar.setFont(new java.awt.Font("Rockwell", 1, 24)); // NOI18N
        lblTitileBar.setForeground(new java.awt.Color(255, 255, 255));
        lblTitileBar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitileBar.setText("iTeach for PC");

        lblClose.setBackground(new java.awt.Color(0, 0, 0));
        lblClose.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblClose.setForeground(new java.awt.Color(102, 102, 102));
        lblClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Close_Window_28px.png"))); // NOI18N
        lblClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblCloseMouseReleased(evt);
            }
        });

        lblMinimize.setBackground(new java.awt.Color(0, 0, 0));
        lblMinimize.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblMinimize.setForeground(new java.awt.Color(102, 102, 102));
        lblMinimize.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMinimize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Minimize_Window_28px.png"))); // NOI18N
        lblMinimize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblMinimizeMouseReleased(evt);
            }
        });

        lblUser.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        lblUser.setForeground(new java.awt.Color(255, 255, 255));
        lblUser.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Male_User_28px_1.png"))); // NOI18N
        lblUser.setText("Hi, iTeach");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTitileBar, javax.swing.GroupLayout.PREFERRED_SIZE, 618, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblUser, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblMinimize, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(lblClose, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                .addComponent(lblTitileBar, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblUser))
            .addComponent(lblClose, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(lblMinimize, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel8.setBackground(new java.awt.Color(0, 0, 51));
        jPanel8.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        copyrights.setBackground(new java.awt.Color(255, 255, 255));
        copyrights.setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        copyrights.setForeground(new java.awt.Color(255, 255, 255));
        copyrights.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        copyrights.setText("© YEAR  iTeach™ Products. All right reserved ");
        jPanel8.add(copyrights, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 230, 41));

        jLabel8.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Local Server   :");
        jPanel8.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 10, 90, -1));

        hostNameText.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        hostNameText.setForeground(new java.awt.Color(255, 255, 255));
        hostNameText.setText("iTeach PC");
        jPanel8.add(hostNameText, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 10, 200, -1));

        AM_PM.setBackground(new java.awt.Color(255, 255, 255));
        AM_PM.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 10)); // NOI18N
        AM_PM.setForeground(new java.awt.Color(255, 255, 255));
        AM_PM.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        AM_PM.setText("A/P");
        jPanel8.add(AM_PM, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 10, 30, 20));

        Clock.setBackground(new java.awt.Color(255, 255, 255));
        Clock.setFont(new java.awt.Font("Digital-7", 0, 21)); // NOI18N
        Clock.setForeground(new java.awt.Color(255, 255, 255));
        Clock.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Clock.setText("00:00:00");
        jPanel8.add(Clock, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 10, 110, 20));

        Date.setBackground(new java.awt.Color(255, 255, 255));
        Date.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        Date.setForeground(new java.awt.Color(255, 255, 255));
        Date.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Date.setText("Date");
        jPanel8.add(Date, new org.netbeans.lib.awtextra.AbsoluteConstraints(920, 0, 70, 30));

        Month.setBackground(new java.awt.Color(255, 255, 255));
        Month.setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        Month.setForeground(new java.awt.Color(255, 255, 255));
        Month.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Month.setText("Month");
        jPanel8.add(Month, new org.netbeans.lib.awtextra.AbsoluteConstraints(920, 20, 70, 20));

        jPanelMain.setBackground(new java.awt.Color(102, 102, 102));

        javax.swing.GroupLayout jPanelMainLayout = new javax.swing.GroupLayout(jPanelMain);
        jPanelMain.setLayout(jPanelMainLayout);
        jPanelMainLayout.setHorizontalGroup(
            jPanelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanelMainLayout.setVerticalGroup(
            jPanelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 495, Short.MAX_VALUE)
        );

        jPanelBar.setBackground(new java.awt.Color(0, 0, 51));

        javax.swing.GroupLayout jPanelBarLayout = new javax.swing.GroupLayout(jPanelBar);
        jPanelBar.setLayout(jPanelBarLayout);
        jPanelBarLayout.setHorizontalGroup(
            jPanelBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanelBarLayout.setVerticalGroup(
            jPanelBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 48, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanelMain, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelBar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanelBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jPanelMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(0, 0, 0)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 584, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jPanel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel5MouseClicked

    private void jPanel11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel11MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel11MouseClicked

    private void lblCloseMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCloseMouseReleased
        System.exit(0);
        System.gc();
    }//GEN-LAST:event_lblCloseMouseReleased

    private void lblMinimizeMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblMinimizeMouseReleased
        this.setState(MainWindow.ICONIFIED);
    }//GEN-LAST:event_lblMinimizeMouseReleased

    private void jPanel13MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel13MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel13MouseClicked

    private void lblStudentMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblStudentMouseClicked
        LoadWindow.setStudentTitle();
    }//GEN-LAST:event_lblStudentMouseClicked

    private void lblStudentIconMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblStudentIconMouseClicked
        LoadWindow.setStudentTitle();
    }//GEN-LAST:event_lblStudentIconMouseClicked

    private void lblStudentIconMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblStudentIconMouseEntered
        ImageIcon student = new ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Students_32px.png"));
        lblStudentIcon.setIcon(student);
        lblStudent.setForeground(Color.red);
    }//GEN-LAST:event_lblStudentIconMouseEntered

    private void lblStudentMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblStudentMouseEntered
        ImageIcon student = new ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Students_32px.png"));
        lblStudentIcon.setIcon(student);
        lblStudent.setForeground(Color.red);
    }//GEN-LAST:event_lblStudentMouseEntered

    private void lblStudentMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblStudentMouseExited
        ImageIcon student = new ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Students_28px.png"));
        lblStudentIcon.setIcon(student);
        lblStudent.setForeground(Color.GRAY);
    }//GEN-LAST:event_lblStudentMouseExited

    private void lblStudentIconMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblStudentIconMouseExited
        ImageIcon student = new ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Students_28px.png"));
        lblStudentIcon.setIcon(student);
        lblStudent.setForeground(Color.GRAY);
    }//GEN-LAST:event_lblStudentIconMouseExited

    private void lblPaymentIconMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPaymentIconMouseEntered
        ImageIcon payment = new ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Card_Payment_32px.png"));
        lblPaymentIcon.setIcon(payment);
        lblPayment.setForeground(Color.red);     }//GEN-LAST:event_lblPaymentIconMouseEntered

    private void lblPaymentIconMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPaymentIconMouseExited
        ImageIcon payment = new ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Card_Payment_28px.png"));
        lblPaymentIcon.setIcon(payment);
        lblPayment.setForeground(Color.GRAY);    }//GEN-LAST:event_lblPaymentIconMouseExited

    private void lblPaymentMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPaymentMouseEntered
        ImageIcon payment = new ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Card_Payment_32px.png"));
        lblPaymentIcon.setIcon(payment);
        lblPayment.setForeground(Color.red);     }//GEN-LAST:event_lblPaymentMouseEntered

    private void lblPaymentMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPaymentMouseExited
        ImageIcon payment = new ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Card_Payment_28px.png"));
        lblPaymentIcon.setIcon(payment);
        lblPayment.setForeground(Color.GRAY);     }//GEN-LAST:event_lblPaymentMouseExited

    private void lblProfileMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblProfileMouseEntered
        ImageIcon user = new ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Change_User_32px_1.png"));
        lblUser1Icon.setIcon(user);
        lblProfile.setForeground(Color.red);
    }//GEN-LAST:event_lblProfileMouseEntered

    private void lblProfileMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblProfileMouseExited
        ImageIcon user = new ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Change_User_28px.png"));
        lblUser1Icon.setIcon(user);
        lblProfile.setForeground(Color.GRAY);    }//GEN-LAST:event_lblProfileMouseExited

    private void lblProfileMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblProfileMouseClicked
        LoadWindow.setEmployeeTitle();
    }//GEN-LAST:event_lblProfileMouseClicked

    private void lblPaymentMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPaymentMouseClicked
        LoadWindow.setPaymentTitle();
    }//GEN-LAST:event_lblPaymentMouseClicked

    private void lblPaymentIconMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPaymentIconMouseClicked
        LoadWindow.setPaymentTitle();
    }//GEN-LAST:event_lblPaymentIconMouseClicked

    private void lblUser1IconMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblUser1IconMouseExited
        ImageIcon user = new ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Change_User_28px.png"));
        lblUser1Icon.setIcon(user);
        lblProfile.setForeground(Color.GRAY);
    }//GEN-LAST:event_lblUser1IconMouseExited

    private void lblUser1IconMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblUser1IconMouseEntered
        ImageIcon user = new ImageIcon(getClass().getResource("/resources/images/icons/mainwindow/icons8_Change_User_32px_1.png"));
        lblUser1Icon.setIcon(user);
        lblProfile.setForeground(Color.red);
    }//GEN-LAST:event_lblUser1IconMouseEntered

    private void lblUser1IconMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblUser1IconMouseClicked
        LoadWindow.setEmployeeTitle();
    }//GEN-LAST:event_lblUser1IconMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel AM_PM;
    private javax.swing.JLabel Clock;
    private javax.swing.JLabel Date;
    private javax.swing.JLabel Month;
    private javax.swing.JLabel copyrights;
    private javax.swing.JLabel hostNameText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    public static javax.swing.JPanel jPanelBar;
    public static javax.swing.JPanel jPanelMain;
    private javax.swing.JLabel lblClose;
    private javax.swing.JLabel lblFacebook;
    private javax.swing.JLabel lblGoogle;
    private javax.swing.JLabel lblMinimize;
    private javax.swing.JLabel lblPayment;
    private javax.swing.JLabel lblPaymentIcon;
    private javax.swing.JLabel lblProfile;
    private javax.swing.JLabel lblStudent;
    private javax.swing.JLabel lblStudentIcon;
    private javax.swing.JLabel lblTitileBar;
    private javax.swing.JLabel lblTwitter;
    private javax.swing.JLabel lblUser;
    private javax.swing.JLabel lblUser1Icon;
    private javax.swing.JLabel lblWeb;
    private javax.swing.JLabel lblYouTube;
    // End of variables declaration//GEN-END:variables

}

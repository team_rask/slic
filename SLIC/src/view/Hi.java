package view;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import methords.StartMethords;

/**
 *
 * @author Dasun
 */
@SuppressWarnings("serial")
public class Hi extends javax.swing.JFrame {

    /**
     * Creates new form Hi
     */
    public Hi() {
        initComponents();
        runTimeAutoSet();
        ImageIcon logo = new ImageIcon(getClass().getResource("/resources/images/logo/logo.png"));
        this.setIconImage(logo.getImage());
        setBackground(new Color(0, 0, 0, 0));
        String name = StartMethords.loginUserName();
        if (name == null) {
            JOptionPane.showMessageDialog(this, "Please Login", "ERROR", 0);
            dispose();
        }
        new Thread(new Runnable() {

            public void run() {
                int createNewDatabase = 0;
                for (int i = 0; i < 100; i++) {

                    if (i == 12) {
                        lb.setText("Connecting to " + name + "'s profile...");
                    }
                    if (i == 25) {
                        lb.setText("Sucsessfuly Connnected...");
                    }
                    if (i == 35) {
                        lb.setText("Loading Moduels...");
                    }
                    if (i == 55) {
                        lb.setText("Create " + name + "'s Dashboard...");
                    }
                    if (i == 80) {
                        lb.setText("Finishing.......");
                    }
                    if (i == 99) {
                        new MainWindow().setVisible(true);
                        dispose();
                    }
                    try {
                        Thread.sleep(40);
                    } catch (InterruptedException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }
        }).start();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jXBusyLabel1 = new org.jdesktop.swingx.JXBusyLabel();
        copyrights = new javax.swing.JLabel();
        userIcon = new javax.swing.JLabel();
        UserNameLable = new javax.swing.JLabel();
        lb = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Loading...");
        setUndecorated(true);

        jPanel1.setOpaque(false);
        jPanel1.setLayout(null);

        jXBusyLabel1.setForeground(new java.awt.Color(153, 153, 0));
        jXBusyLabel1.setBusy(true);
        jPanel1.add(jXBusyLabel1);
        jXBusyLabel1.setBounds(310, 210, 30, 40);

        copyrights.setBackground(new java.awt.Color(255, 255, 255));
        copyrights.setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        copyrights.setForeground(new java.awt.Color(204, 204, 204));
        copyrights.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        copyrights.setText("© YEAR iTeach™ Products. All right reserved ");
        jPanel1.add(copyrights);
        copyrights.setBounds(280, 520, 330, 30);

        userIcon.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel1.add(userIcon);
        userIcon.setBounds(410, 430, 60, 60);

        UserNameLable.setBackground(new java.awt.Color(255, 255, 255));
        UserNameLable.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        UserNameLable.setForeground(new java.awt.Color(255, 255, 255));
        UserNameLable.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        UserNameLable.setToolTipText("");
        jPanel1.add(UserNameLable);
        UserNameLable.setBounds(110, 490, 670, 40);

        lb.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        lb.setForeground(new java.awt.Color(255, 255, 255));
        lb.setText("Starting....");
        jPanel1.add(lb);
        lb.setBounds(340, 210, 407, 40);

        jLabel2.setFont(new java.awt.Font("Arial", 0, 150)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/icons/login/Hi.png"))); // NOI18N
        jPanel1.add(jLabel2);
        jLabel2.setBounds(0, 0, 900, 700);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 907, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 701, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Hi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Hi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Hi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Hi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Hi().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel UserNameLable;
    private javax.swing.JLabel copyrights;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private org.jdesktop.swingx.JXBusyLabel jXBusyLabel1;
    private javax.swing.JLabel lb;
    private javax.swing.JLabel userIcon;
    // End of variables declaration//GEN-END:variables

    private void runTimeAutoSet() {
        String thisYear = getYear();
        copyrights.setText("© " + thisYear + " iTeach™ Products. All right reserved ");
    }

    //Get This Year
    public static String getYear() {
        SimpleDateFormat year = new SimpleDateFormat("YYYY");
        String thisYear = (year.format(new Date()));
        return thisYear;
    }
}
